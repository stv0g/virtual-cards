<!--
SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: CC-BY-4.0
-->

# Container image of the FluffyKaon OpenPGP card applet

Different variants of the FluffyPGP applet exist online.
This image uses: https://github.com/FluffyKaon/OpenPGP-Card

Note that there is a variant with slightly newer commit timestamps at: https://github.com/JavaCardOS/FluffyPGP-Applet.
However, that version is lacking
[this patch](https://github.com/FluffyKaon/OpenPGP-Card/commit/729c68bc877cdb8321e5be3a29f6bc385645fd10).
Without the patch, the applet doesn't perform correctly with the tests in https://gitlab.com/openpgp-card/openpgp-card/.