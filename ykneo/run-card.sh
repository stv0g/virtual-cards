# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

echo "Starting jcardsim" 1>&2
java -cp ./jcardsim/target/jcardsim-3.0.5-SNAPSHOT.jar:./ykneo-openpgp/applet/src com.licel.jcardsim.remote.VSmartCard jcardsim.cfg &
echo "Started jcardsim" 1>&2

sleep 10

echo "Running opensc-tool" 1>&2
opensc-tool --card-driver default --send-apdu 80b800002210D276000124010200000611112222000010D276000124010200000611112222000000 1>&2
echo "Done running opensc-tool" 1>&2
