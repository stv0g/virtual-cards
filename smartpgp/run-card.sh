# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

echo "Starting jcardsim" 1>&2
java -cp ./jcardsim/target/jcardsim-3.0.5-SNAPSHOT.jar:./SmartPGP/src/ com.licel.jcardsim.remote.VSmartCard jcardsim.cfg  &
echo "Started jcardsim" 1>&2

echo "Running opensc-tool" 1>&2
start="$(date +%s)"
timeout=20
while sleep 1 && ! opensc-tool --card-driver default --send-apdu 80b800002210D276000124010200afaf00001234000010D276000124010200afaf00001234000000 1>&2; do
	now="$(date +%s)"
	duration="$(expr "$now" - "$start")"
	if [ "$duration" -gt "$timeout" ]; then
		echo "it took longer than $timeout seconds for virtual card to start" 1>&2
		exit 1
	fi
done

echo "Done running opensc-tool" 1>&2
