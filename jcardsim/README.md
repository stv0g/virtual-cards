<!--
SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: CC-BY-4.0
-->

# jCardSim 

Container image of https://github.com/licel/jcardsim

Based on documentation at
https://github-wiki-see.page/m/OpenSC/OpenSC/wiki/Smart-Card-Simulation

## Use with vsmartcard

We're using the fork https://github.com/arekinath/jcardsim that allows use of jCardSim with the `vsmartcard` framework.

## Change to the upstream sources

This image patches the code from upstream so that ByteContainer doesn't fail when a differently sized buffer is set.
See: https://github.com/ANSSI-FR/SmartPGP/issues/40#issuecomment-945172191
