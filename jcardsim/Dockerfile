# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

FROM registry.gitlab.com/openpgp-card/virtual-cards/vsmartcard

RUN useradd -ms /bin/bash jcardsim \
 && mkdir -p /usr/share/man/man1 \
 && apt update -y -qq \
 && apt install -y -qq ant maven openjdk-17-jdk \
 && apt clean

USER jcardsim
WORKDIR /home/jcardsim

# Use maven-antrun-plugin 3.0.0, to be able to build
# with more recent jdk versions (jdk 11+ doesn't
# contain tools.jar)
COPY jcardsim/pom.patch .

# Allow changing of data size in
# ByteContainer.setBytes(byte[], short, short).
#
# Also see https://github.com/ANSSI-FR/SmartPGP/issues/40
COPY jcardsim/bytecontainer.patch .

RUN git clone https://github.com/arekinath/jcardsim.git \
 && patch jcardsim/pom.xml pom.patch \
 && patch jcardsim/src/main/java/com/licel/jcardsim/crypto/ByteContainer.java bytecontainer.patch\
 && cd jcardsim/ \
 && git submodule add https://github.com/martinpaljak/oracle_javacard_sdks sdks \
 && export JC_CLASSIC_HOME=sdks/jc305u3_kit \
 && mvn initialize && mvn clean install \
 && true

