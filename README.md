<!--
SPDX-FileCopyrightText: 2021-2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: CC-BY-4.0
-->

# Container images for OpenPGP card testing

This repository offers container images for easy testing of software that interacts with
[OpenPGP cards](https://en.wikipedia.org/wiki/OpenPGP_card), without needing to use physical hardware tokens.
Instead, these images provide "virtual OpenPGP cards" that can be used in environments like GitLab CI.

The images are used in the CI infrastructure of the [openpgp-card](https://gitlab.com/openpgp-card/openpgp-card) project.

They are also convenient for testing of any other software that interacts with OpenPGP cards,
including applications built on top of the `openpgp-card` libraries.

**The virtual OpenPGP cards in these images offer no meaningful protection for private key material!**
The only purpose for these images is easy testing of software that makes use of OpenPGP cards.

# OpenPGP card implementations

Currently, container images for four OpenPGP card implementations are provided:

## SmartPGP

[SmartPGP](https://github.com/ANSSI-FR/SmartPGP/) is a modern OpenPGP card implementation, up to date with the latest
version 3.4 of the OpenPGP card specification. Development is ongoing.

We currently package version v1.22.1-3.0.4 of SmartPGP.

SmartPGP supports RSA with 2048, 3072 and 4096 bit key sizes, as well as ECC algorithms NIST-P and brainpool.
[Curve 25519 is not yet available](https://github.com/ANSSI-FR/SmartPGP/issues/10)
(existing Java Card 3.0.x hardware doesn't support the algorithm).

Our image of SmartPGP presents with the following identity:

```
OpenPGP card AFAF:00001234 (card version 2.0)
Application Identifier: D276000124 01 01 0200 AFAF 00001234 0000
```

## opcard-rs

[Nitrokey Rust implementation of OpenPGP card](https://github.com/Nitrokey/opcard-rs)

This is a modern OpenPGP card implementation, based on version 3.4 of the card specification.
The implementation is currently in alpha testing, right now it supports Curve25519 and NIST P-256 keys.

This implementation will be used on the Nitrokey 3.

Our opcard-rs image presents with the following identity:

```
OpenPGP card 0000:00000000 (card version 3.4)
Application Identifier: D276000124 01 01 0304 0000 00000000 0000
```

## CanoKey

Implementation of OpenPGP card by https://canokeys.org (also see https://github.com/canokeys/canokey-core).

This is a modern OpenPGP card implementation, written largely in C, based on version 3.4 of the card specification.
The implementation supports a wide range of algorithms (RSA2048, 3072, 4096; NIST-P 256, 384; Secp256k1;
Curve 25519, as well as the "State Cryptography Administration of China" compliant SM234 algorithms).

The OpenPGP application of our CanoKey image presents with the following identity:

```
OpenPGP card F1D0:00000000
Application Identifier: D276000124 01 0304 F1D0 00000000 0000
```

## YubiKey NEO

The [YubiKey NEO](https://github.com/Yubico/ykneo-openpgp/) applet consists of the code that also runs on the 
physical YubiKey NEO hardware token.

Development ended roughly in 2015 (at that time, the OpenPGP card specification 2.1 was current).

The applet supports only one algorithm: RSA 2048.

Our YubiKey NEO image presents with the following identity:

```
OpenPGP card 0006:11112222 (card version 2.0)
Application Identifier: D276000124 01 01 0200 0006 11112222 0000
```

## Fluffy PGP

[FluffyPGP](https://github.com/FluffyKaon/OpenPGP-Card/) is an implementation of the OpenPGP card 2.0
specification. The implementation is from 2013 (when the OpenPGP card specification 2.0.1 was current).

The applet supports only one algorithm: RSA 2048.

Our FluffyPGP card image presents with the following identity:

```
OpenPGP card FFF1:00000001 (card version 2.0)
Application Identifier: D276000124 01 01 0200 FFF1 00000001 0000
```

# How to use these images

## Choosing the appropriate image

This repository offers two images of each OpenPGP card implementation.
The variants differ in the amount of pre-installed software:

- Images with just the software needed to run each virtual OpenPGP card:
  - `registry.gitlab.com/openpgp-card/virtual-cards/smartpgp`
  - `registry.gitlab.com/openpgp-card/virtual-cards/opcard-rs`
  - `registry.gitlab.com/openpgp-card/virtual-cards/canokey`
  - `registry.gitlab.com/openpgp-card/virtual-cards/ykneo`
  - `registry.gitlab.com/openpgp-card/virtual-cards/fluffypgp`
- Images with additional software packages for building and running Rust software and Sequoia PGP:
  - `registry.gitlab.com/openpgp-card/virtual-cards/smartpgp-builddeps`
  - `registry.gitlab.com/openpgp-card/virtual-cards/opcard-rs-tools`
  - `registry.gitlab.com/openpgp-card/virtual-cards/canokey-builddeps`
  - `registry.gitlab.com/openpgp-card/virtual-cards/ykneo-builddeps`
  - `registry.gitlab.com/openpgp-card/virtual-cards/fluffypgp-builddeps`

The second set of images are particularly convenient when testing projects that are based on `openpgp-card`.

## Starting the smart cards

With any of these images, you can start `pcscd` and the smart card implementation by running the helper script:

```
sh /start.sh
```

At this point, the smart card is accessible for use in the container. You can then run software
that uses the virtual OpenPGP card, such as a test-suite.

## Interactive use in a container

To explore how these images work, we'll run an interactive container and interact with the virtual card in that 
container.

If you are planning to build CI tests for your software project, you can analogously experiment with your tests in an
interactive container.

So, first we start a (temporary) container based on the SmartPGP image with Rust/Sequoia PGP dependencies pre-installed:

```
$ docker run --rm -it registry.gitlab.com/openpgp-card/virtual-cards/smartpgp-builddeps
[..]
```

In this container, we now start the simulated OpenPGP card (running the script will take a few seconds):

```
# sh /start.sh
[..]
```

Note: the `start.sh` script redirected the output of jcardsim-based cards to `/dev/null`.
If we didn't redirect this output, we would see a lot of (usually distracting) debug output of the Java Card Simulator.

(At this point, you could experiment with running the test suite of your project against the virtual SmartPGP card,
after `git clone`-ing your project into the container.)

To see the virtual card in action, here we'll generate a set of keys on the virtual card, and then inspect the
card's status:

1. Install the [opgpcard](https://openpgp-card.gitlab.io/guide/opgpcard.html) CLI tool to interact with the 
virtual OpenPGP card:

```
# cargo install openpgp-card-tools
[..]
# export PATH=/root/.cargo/bin:$PATH
```

2.  Generate a set of keys on the card (enter "12345678" as the Admin PIN, and "123456" as the User PIN):

```
# opgpcard admin -c AFAF:00001234 generate nistp521
Enter Admin PIN:
Enter User PIN:
 Generate subkey for Signing
 Generate subkey for Decryption
 Generate subkey for Authentication
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: 82CB 82DF 30DB DD00 D220  71FA 1F53 37CE 2CE1 C43E
[..]
```

3. Inspect the resulting state of our virtual card:

```
# opgpcard status
OpenPGP card AFAF:00001234 (card version 2.0)

Cardholder:
Language preferences: 'en'

Signature key
  fingerprint: 82CB 82DF 30DB DD00 D220  71FA 1F53 37CE 2CE1 C43E
  created: 2022-05-16 16:00:23
  algorithm: NistP521r1 (ECDSA)

Decryption key
  fingerprint: 3C92 2C9E 6F30 5BAE 0173  2CA2 5484 D981 4700 5F67
  created: 2022-05-16 16:00:23
  algorithm: NistP521r1 (ECDH)

Authentication key
  fingerprint: 87C9 5AAC A1E7 8602 4646  33CE 3F58 09BC B374 2AFD
  created: 2022-05-16 16:00:23
  algorithm: NistP521r1 (ECDSA)

Signatures made: 3

Remaining tries: User PIN: 3, Admin PIN: 3, Reset Code: 0
Signature PIN only valid once: true
```

4. We can also inspect the virtual card with GnuPG:

```
# apt install scdaemon
[..]
# gpg --card-status
Reader ...........: Virtual PCD 00 00
Application ID ...: D276000124010200AFAF000012340000
Application type .: OpenPGP
Version ..........: 2.0
Manufacturer .....: test card
Serial number ....: 00001234
Name of cardholder: [not set]
Language prefs ...: en
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: forced
Key attributes ...: nistp521 nistp521 nistp521
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 3
KDF setting ......: off
Signature key ....: 82CB 82DF 30DB DD00 D220  71FA 1F53 37CE 2CE1 C43E
      created ....: 2022-05-16 16:00:23
Encryption key....: 3C92 2C9E 6F30 5BAE 0173  2CA2 5484 D981 4700 5F67
      created ....: 2022-05-16 16:00:23
Authentication key: 87C9 5AAC A1E7 8602 4646  33CE 3F58 09BC B374 2AFD
      created ....: 2022-05-16 16:00:23
General key info..: [none]
```

We see that our containerized virtual card is also visible to GnuPG, just like a physical card would be.

Just like with physical OpenPGP cards, GnuPG's `scdaemon` will by default open cards with exclusive access, which
[prevents other software from using these cards](https://openpgp-card.gitlab.io/guide/opgpcard.html#troubleshooting-error-no-cards-found).

So, after checking the card with `gpg --card-status`, other tools (including `opgpcard`) can't access the virtual
card anymore, until `scdaemon` yields access. The recommended way to shut down scdaemon is: `gpgconf --kill scdaemon`.

## Use in the openpgp-card project

To see how the `openpgp-card` project uses these images in CI tests, you can look at the
[gitlab CI config](https://gitlab.com/openpgp-card/openpgp-card/-/blob/main/.gitlab-ci.yml)
and the
[virtual card test configurations](https://gitlab.com/openpgp-card/openpgp-card/-/tree/main/card-functionality/ci/).

## Which virtual cards should I test against?

### Short answer

Many high level software projects that make use of OpenPGP cards will probably want to test against just SmartPGP,
in CI. It is currently the most complete and modern containerized OpenPGP card implementation. 

### Longer answer

As a library, `openpgp-card` strives to provide reliable abstractions for other software projects to build on. This
includes handling possible card-specific peculiarities - ideally in a way that hides resulting complexity from
client software. Thus, for `openpgp-card` itself, one goal is to
[test against as many OpenPGP card implementations as possible](https://sequoia-pgp.org/blog/2021/12/20/202112-openpgp-card-ci/).
Virtualized cards are particularly convenient for this.

Software that uses the `openpgp-card` libraries to interact with OpenPGP cards on the other hand, should not need
to worry as much about differences between models of cards.
However, some differences between OpenPGP card implementations cannot be abstracted away.
For example, cards differ in which algorithms they support (and only modern cards provide a list of supported
algorithms). There are also various optional features that some implementations provide, and others do not.
On top of that, some devices implement vendor-specific proprietary extensions to the standard.

For many software projects that use OpenPGP cards to perform simple signing or decryption operations, testing against
just one card implementation is sufficient. In such cases, you'll probably want to test against SmartPGP.

If your project's use of OpenPGP cards is more complex, differences in the feature-set of cards could cause your
software to fail in ways that a test suite can detect. In such cases, testing your code against at least SmartPGP and
YubiKey NEO is probably worth the effort (FluffyPGP is very similar to the YubiKey NEO from the perspective of client
software).

### You should probably also test against hardware cards

Testing against virtual cards is a great first line of defense against bugs.
It can be automated, and gives you quick feedback during development, if something is broken.
However, you'll probably also want to test against physical cards of the models that you want to support.
This project cannot help you with that.

### Gnuk: virtualizable, but not in typical CI environments

If your test environment allows you to load kernel modules, you also have the option to test against an
[emulated Gnuk OpenPGP card](https://gitlab.com/openpgp-card/openpgp-card#emulated-gnuk).

[Gnuk](https://www.fsij.org/doc-gnuk/) is a modern Free Software implementation of the OpenPGP card standard.
It supports Curve 25519.

However, running 'emulated Gnuk' relies on [USB/IP](http://usbip.sourceforge.net/), which requires loading
a kernel module. So unfortunately emulated Gnuk can't be run in typical CI environments.

# Technical details

The stack of software when using one of these OpenPGP card images looks like this:

```mermaid
graph BT
    PCSC["pcscd <br/> (PC/SC daemon)"] --> VSC
    VSC["vsmartcard <br/> (Virtual Smart Card Architecture)"] --> JCS["jCardSim <br/> Java Card Runtime Environment Simulator"]
    JCS --> SPGP["SmartPGP"] -.-> TEST["Test suite using virtual OpenPGP card"]
    JCS --> YKNEO["YubiKey NEO"] -.-> TEST
    JCS --> FPGP["Fluffy PGP"] -.-> TEST
    VSC --> OPC["opcard-rs"] -.-> TEST["Test suite using virtual OpenPGP card"]


click VSC "https://frankmorgner.github.io/vsmartcard/virtualsmartcard/README.html"
click JCS "https://jcardsim.org/"
click SPGP "https://github.com/ANSSI-FR/SmartPGP/"
click YKNEO "https://github.com/Yubico/ykneo-openpgp/"
click FPGP "https://github.com/FluffyKaon/OpenPGP-Card/"
click OPC "https://github.com/Nitrokey/opcard-rs/"

classDef userApp fill:#f8f8f8,stroke-dasharray: 5 5;
class UA userApp;
```

The images in this repository use the following OpenPGP card implementations:

- [SmartPGP](https://github.com/ANSSI-FR/SmartPGP/)
- [opcard-rs](https://github.com/Nitrokey/opcard-rs/)
- [YubiKey NEO](https://github.com/Yubico/ykneo-openpgp/)
- [FluffyPGP](https://github.com/FluffyKaon/OpenPGP-Card/)

Two additional projects allow us to run these OpenPGP card applets and connect them with `pcscd`:

- [jCardSim](https://jcardsim.org/): a Java Card Runtime Environment Simulator.  
  OpenPGP card applets can run in this simulated Java Card Environment
  (SmartPGP, YubiKey NEO and FluffyPGP are Java Card applets).
  
- [vsmartcard](https://frankmorgner.github.io/vsmartcard/virtualsmartcard/README.html):
  Virtual Smart Card Architecture. Provides infrastructure for running emulated smart cards.  
  Our version of jCardSim uses this emulation layer to connect to `pcscd`. 

## Notes on jCardSim

The original upstream jCardSim project sources live at
https://github.com/licel/jcardsim.

However, our jCardSim image uses the fork at https://github.com/arekinath/jcardsim, which adds support for
`vsmartcard/vpcd` integration.

Additionally, two patches on top of `arekinath/jcardsim` are currently
applied while building the jCardSim image:
- [Allow building jCardSim with JDK 11+](https://gitlab.com/openpgp-card/virtual-cards/-/blob/main/jcardsim/pom.patch)
- [Hack to make NIST P521 work in SmartPGP](https://gitlab.com/openpgp-card/virtual-cards/-/blob/main/jcardsim/bytecontainer.patch)
  (see https://github.com/ANSSI-FR/SmartPGP/issues/40)
